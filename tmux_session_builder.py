#!/usr/bin/env python3
""" Create a tmux session script interactively, dumped to a file.

Asks yes/no questions whether a specific command is to be executed on creation,
which are scattered acros windows. The result is then written to a file.

Usage example within a bash script:

    ####################
    #!/bin/bash
    TMUXSCRIPT='/tmp/tmux_session_script.sh'
    NAME='hello'
    JSON='{
        "abc": [
            ["ls", "ls -a"]
        ],
        "def": [
            ["firefox", "firefox && exit"],
            ["thunderbird", "thunderbird && exit"]
        ]
    }'

    python3 ~/bin/tmux_session_builder.py "$TMUXSCRIPT" "$NAME" "$JSON" && chmod u+x "$TMUXSCRIPT" && "$TMUXSCRIPT" && rm "$TMUXSCRIPT"
    ####################
"""
import collections
import json
import sys


def request_yes_no(question, true_on_empty=True, false_on_fail=False):
    addendum = ' [Y/n]: ' if true_on_empty  else ' [y/n]: '
    while True:
        answer = input(question + addendum )
        if true_on_empty and answer is '':
            return True
        if answer in ['y', 'ye', 'yes', 'Y', 'YE', 'YES']:
            return True
        if answer in ['n', 'no', 'N', 'NO']:
            return False
        if false_on_fail:
            return False
        print('Invalid input')


def ask_to_select_commands(question_and_commands):
    accepted_commands = []
    for question, command in question_and_commands:
        if request_yes_no(question):
            accepted_commands.append(command)
    return accepted_commands


def build_window_lines(commands, window_name, session_name):
    n_com = len(commands)
    lines = []
    lines.append('# ' + window_name + ' has ' + str(n_com) + ' subwindows')
    if n_com > 0:
        lines.append('tmux new-window -n ' + window_name)
    for i in range(0,n_com-1):
        lines.append('tmux split-window -v -t ' \
                        + subwindow_accessor(session_name, window_name, i))
    for i, command in enumerate(commands):
        lines.append('tmux send-keys -t ' \
                        + subwindow_accessor(session_name, window_name, i) \
                        + ' \'' +  commands[i] + '\' Enter')
    lines.append('')
    return lines


def subwindow_accessor(session_name, window_name, i):
    """E.g. session:window.0"""
    return session_name + ':' + window_name + '.' + str(i)


def build_lines(session_name, qncs_by_window_name):
    lines = []
    lines.append('#!/bin/bash')
    lines.append('#')
    lines.append('# set up a tmux session with automatically started programs')
    lines.append('tmux -2 new-session -s ' + session_name + ' -d')
    lines.append('')
    for window_name, qncs in qncs_by_window_name.items():
        accepted_commands = ask_to_select_commands(qncs)
        lines.extend(
            build_window_lines(
                accepted_commands, window_name, session_name
        ))
    lines.append('tmux select-window -t ' + session_name + ':0')
    lines.append('tmux attach-session -t ' + session_name)
    return lines


def main(dump_file, session_name, qnas_by_window_name):
    """Create a tmux session script by interactively asking questions
    :param dump_file: name of a file to write to
    :param session_name: name of the tmux session to use
    :param qnas_by_window_name: Dictionary. Key is a tmux window name,
    value is a list. Each item of the list is again a list with exactly
    two elements: the first one is the question to ask to the user,
    the second one is the command that will be used inside a split window.

    The command strings should not contain a ', as those are used to
    enclose the commands in the bash script.
    """
    lines = build_lines(session_name, qnas_by_window_name)
    script = '\n'.join(lines)
    with open(dump_file,'w') as script_file:
        script_file.write(script)


def example_main():
    dump_file = '/tmp/tmux_session_script.sh'
    session_name = 'test'

    example = collections.OrderedDict()
    example['abc'] = [['ls', 'ls -a']]
    example['def'] = [
        ['firefox', "firefox && exit"],
        ['thunderbird', "thunderbird && exit"],
    ]

    main(dump_file, session_name, example)


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('Usage: python3 tmux_session_builder FILE NAME JSON')
        print('Create a tmux session script by interactively asking questions')
        print()
        print('FILE: file location to write the script to')
        print('NAME: name of the tmux session to create')
        print('JSON: a json representative of windows, questions and commands')
        print('      Example:')
        print('          {')
        print('              "abc": [')
        print('                  ["ls", "ls -a"]')
        print('              ],')
        print('              "def": [')
        print('                  ["firefox", "firefox && exit"],')
        print('                  ["thunderbird", "thunderbird && exit"]')
        print('              ]')
        print('          }')
        sys.exit(1)

    dump_file = sys.argv[1]
    session_name = sys.argv[2]
    qnas_by_window_name = json.loads(sys.argv[3])
    main(dump_file, session_name, qnas_by_window_name)
