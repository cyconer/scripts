#!/usr/bin/env sh
# Useage:
#   md2html.sh filename.md
# This will output a filename.html
# To avoid unexpected behavior, only use files that are in the same directory
# as this script, without a preceeding relative or absolute path.
#
# Requirements:
#   pandoc

IN="${1}"
FILE_NAME="${IN%%.*}"
OUT="${FILE_NAME}.html"
echo "Converting $IN to $OUT"
pandoc -s -o $OUT $IN
