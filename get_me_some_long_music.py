#!/usr/bin/python3
#
# folder structure:
# ├── downloads                 # where the downloaded files are temporarily stored
# ├── edited                    # where the final files are stored
# ├── get_me_some_long_music.py # this script
# ├── links.txt                 # file containing youtube links
# └── yt_dl.py                  # youtube_dl script; the script in the ubuntu repository is often outdated
#
# requirements: libsox-fmt-mp3 sox


import os

LINKS_FILE = "links.txt"
DOWNLOAD_DIR = "downloads"
EDITED_DIR = "edited"


def read_urls():
    print('### read urls ###')
    with open(LINKS_FILE, 'r') as file:
        lines = file.readlines()
    return [line.rstrip() for line in lines]


def download(url):
    print('### download', url)
    command = './yt_dl.py --extract-audio --audio-format mp3 --output "' + DOWNLOAD_DIR + '/%(title)s.%(ext)s" ' + url
    os.system(command)


def rename_file_without_special_characters(filename):
    """ Convert space to _, and remove ()[]{}\|'" from filename"""
    new_filename = filename.replace(" ", "_")
    for char in ["(", ")", "[", "]", "{", "}", "\\", "|", "'", '"']:
        new_filename = new_filename.replace(char, "")
    if new_filename != filename:
        os.rename(DOWNLOAD_DIR + "/" + filename, DOWNLOAD_DIR + "/" + new_filename)
    return new_filename


def crop(filename):
    print('### crop', filename)
    in_loc = DOWNLOAD_DIR + '/' + filename
    out_loc = EDITED_DIR + '/' + filename
    command = 'sox ' + in_loc + ' ' + out_loc + ' fade 0 15:00 60'
    os.system(command)



if __name__ == '__main__':
    urls = read_urls()
    for url in urls:
        download(url)
    for filename in os.listdir(DOWNLOAD_DIR):
        filename = rename_file_without_special_characters(filename)
        crop(filename)
        os.remove(DOWNLOAD_DIR + '/' + filename)
