# to be sourced, e.g. via ~/.profile

# use right als as compose key
# e.g. rAlt + " + a = ä
# setxkbmap -option "compose:ralt"

# use capslock as left ctrl key
setxkbmap -option "ctrl:nocaps"

# when left ctrl is pressed and released on its own,
# it behaves as an escape key
# xcape -e 'Control_L=Escape'
